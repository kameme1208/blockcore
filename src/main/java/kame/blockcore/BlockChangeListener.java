package kame.blockcore;

import kame.blockcore.event.ItemBlockDropItemEvent;
import kame.blockcore.event.ItemBlockFallingEvent;
import kame.blockcore.event.ItemBlockLandingEvent;
import kame.tagapi.TagBase;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;
import org.bukkit.craftbukkit.v1_19_R1.block.impl.CraftFluids;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

class BlockChangeListener implements Listener {

    private final Map<UUID, ItemStack> falls = new HashMap<>();

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void onPlace(BlockPlaceEvent event) {
        if (!event.getItemInHand().getType().isBlock()) {
            return;
        }
        var item = event.getItemInHand().clone();
        var tag = TagBase.from(item).getSection();
        if (tag != null && tag.containsKey(BlockAPI.TAG_NAME)) {
            item.setAmount(1);
            BlockAPI.registerBlock(event.getBlock(), item);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void onBlockBreak(BlockBreakEvent event) {
        var block = event.getBlock();
        var item = BlockAPI.unregisterBlock(block);
        if (item.isPresent() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
            var equip = event.getPlayer().getEquipment();
            if (equip != null && !block.getDrops(equip.getItemInMainHand()).isEmpty()) {
                block.getWorld().dropItemNaturally(block.getLocation(), item.get());
            }
            event.setDropItems(false);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void onBlockBreak(BlockExplodeEvent event) {
        event.blockList().removeIf(this::checkInventory);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void onBlockBreak(EntityExplodeEvent event) {
        event.blockList().removeIf(this::checkInventory);
        event.blockList().removeIf(this::dropItem);
    }

    private boolean checkInventory(Block block) {
        return BlockAPI.containsBlock(block) && block.getState() instanceof Container;
    }

    private boolean dropItem(Block block) {
        var item = BlockAPI.unregisterBlock(block);
        item.ifPresent(x -> block.getWorld().dropItemNaturally(block.getLocation(), x));
        item.ifPresent(x -> block.setType(Material.AIR));
        return item.isPresent();
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    private void onBlockBreak(BlockFromToEvent event) {
        var block = event.getToBlock();
        var item = BlockAPI.unregisterBlock(block);
        if (item.isPresent() && event.getBlock().getBlockData() instanceof Levelled base) {
            block.setType(event.getBlock().getType());
            if(block.getBlockData() instanceof Levelled to) {
                block.getWorld().dropItemNaturally(block.getLocation(), item.get());
                to.setLevel(base.getLevel() + 1);
                block.setBlockData(to);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onEntityChangeBlock(EntityChangeBlockEvent event) {
        var entity = event.getEntity();
        var block = event.getBlock();
        if (event.getEntity() instanceof FallingBlock) {
            var uuid = entity.getUniqueId();
            if (entity.isOnGround()) {
                // Landing
                var item = falls.remove(uuid);
                if (item != null) {
                    BlockAPI.registerBlockSilent(block, item);
                    Bukkit.getPluginManager().callEvent(new ItemBlockLandingEvent(entity, block, item));
                }
            } else {
                // Falling
                var item = BlockAPI.unregisterBlockSilent(block);
                if (item.isPresent()) {
                    falls.put(uuid, item.get());
                    Bukkit.getPluginManager().callEvent(new ItemBlockFallingEvent(entity, block, item.get()));
                }
            }
        } else if (BlockAPI.containsBlock(event.getBlock())) {
            event.setCancelled(true);
            block.getState().update();
        }
    }


    @EventHandler(ignoreCancelled = true)
    private void onItemSpawn(EntityDropItemEvent event) {
        var entity = event.getEntity();
        if (entity instanceof FallingBlock) {
            var uuid = entity.getUniqueId();
            var item = falls.remove(uuid);
            if (item != null) {
                event.getItemDrop().setItemStack(item);
                Bukkit.getPluginManager().callEvent(new ItemBlockDropItemEvent(entity, item));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void onPistonMove(BlockPistonExtendEvent event) {
        onPistonMove(event, event.getBlocks());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void onPistonMove(BlockPistonRetractEvent event) {
        onPistonMove(event, event.getBlocks());
    }

    private void onPistonMove(BlockPistonEvent event, List<Block> blocks) {
        var moves = new ArrayList<Block>(blocks.size());
        for (var block : blocks.stream().filter(BlockAPI::containsBlock).toList()) {
            switch (block.getPistonMoveReaction()) {
                case MOVE, BLOCK, IGNORE, PUSH_ONLY -> moves.add(block);
                case BREAK -> {
                    var loc = block.getLocation();
                    for (var player : block.getWorld().getPlayers()) {
                        if (player.getLocation().distance(loc) < 64)
                            player.playEffect(loc, Effect.STEP_SOUND, block.getType());
                    }
                    BlockAPI.unregisterBlock(block).ifPresent(x -> block.getWorld().dropItemNaturally(block.getLocation(), x));
                    block.setType(Material.AIR);
                }
            }
        }
        BlockAPI.moveBlock(moves, event.getDirection());
    }
}
