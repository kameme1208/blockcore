package kame.blockcore;

import kame.blockcore.event.ItemBlockMovedEvent;
import kame.blockcore.event.ItemBlockRegisterEvent;
import kame.blockcore.event.ItemBlockUnregisterEvent;
import kame.tagapi.TagBase;
import kame.tagapi.TagByte;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public final class BlockAPI {

    static final Map<Block, ItemStack> blocks = new HashMap<>();

    public static final String TAG_NAME = "ItemBlockTag";

    public static void registerBlock(@NotNull Block block, @Nullable ItemStack item) {
        registerBlockSilent(block, item);
        Bukkit.getPluginManager().callEvent(new ItemBlockRegisterEvent(block, item));
    }

    public static void registerBlockSilent(@NotNull Block block, @Nullable ItemStack item) {
        blocks.put(block, item);
    }

    public static Optional<ItemStack> getTaggedBlock(@NotNull Block block) {
        return Optional.ofNullable(blocks.get(block));
    }

    public static boolean containsBlock(@NotNull Block block) {
        return blocks.containsKey(block);
    }

    public static void moveBlock(@NotNull List<Block> moves, @NotNull BlockFace direction) {
        var swap = new HashMap<Block, ItemStack>();
        for (var move : moves) {
            if (blocks.containsKey(move)) {
                swap.put(move.getRelative(direction), blocks.remove(move));
            }
        }
        if (!swap.isEmpty()) {
            blocks.putAll(swap);
            Bukkit.getPluginManager().callEvent(new ItemBlockMovedEvent(swap, direction));
        }
    }

    public static Optional<ItemStack> unregisterBlock(@NotNull Block block) {
        var item = unregisterBlockSilent(block);
        item.ifPresent(x -> Bukkit.getPluginManager().callEvent(new ItemBlockUnregisterEvent(block, x)));
        return item;
    }

    public static Optional<ItemStack> unregisterBlockSilent(Block block) {
        return Optional.ofNullable(blocks.remove(block));
    }

    public static boolean addTag(ItemStack item) {
        var tag = TagBase.from(item);
        tag.getSection().put(TAG_NAME, new TagByte((byte)1));
        return tag.save();
    }

    public static boolean removeTag(ItemStack item) {
        var tag = TagBase.from(item);
        tag.getSection().remove(TAG_NAME);
        return tag.save();
    }
}
