package kame.blockcore;

import kame.blockcore.event.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;

public final class BlockCore extends JavaPlugin implements Listener {

    private boolean debugMode;

    @Override
    public void onEnable() {
        // Plugin startup logic
        Bukkit.getPluginManager().registerEvents(new BlockChangeListener(), this);
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length >= 1) {
            switch (args[0]) {
                case "add" -> {
                    if (sender instanceof Player player && player.getEquipment() != null) {
                        var result = BlockAPI.addTag(player.getEquipment().getItemInMainHand());
                        sender.sendMessage("[blockcore] ItemStack is%s changed.".formatted(result ? "§a" : "§c not"));
                    }
                    return true;
                }
                case "remove" -> {
                    if (sender instanceof Player player && player.getEquipment() != null) {
                        var result = BlockAPI.removeTag(player.getEquipment().getItemInMainHand());
                        sender.sendMessage("[blockcore] ItemStack is%s changed.".formatted(result ? "§a" : "§c not"));
                    }
                    return true;
                }
                case "debug" -> {
                    debugMode = !debugMode;
                    sender.sendMessage("set debug mode (%s)".formatted(debugMode));
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        return args.length == 1 ? List.of("add", "remove", "debug") : List.of();
    }

    @EventHandler
    private void onEnable(PluginEnableEvent event) throws IOException {
        if (event.getPlugin() instanceof BlockCore) {
            getLogger().log(Level.INFO, "Loading blocks.save...");
            var time = System.nanoTime();
            var file = new File(getDataFolder(), "blocks.save");
            if (!file.isFile()) new YamlConfiguration().save(file);
            var config = YamlConfiguration.loadConfiguration(file);
            var blocks = config.getConfigurationSection("blocks");
            if (blocks == null) return;
            for (var key : blocks.getKeys(false)) {
                var args = key.split("_");
                var world = Bukkit.getWorld(UUID.fromString(args[0]));
                if (world != null) {
                    var x = Integer.parseInt(args[1]);
                    var y = Integer.parseInt(args[2]);
                    var z = Integer.parseInt(args[3]);
                    var block = world.getBlockAt(x, y, z);
                    var item = blocks.getItemStack(key);
                    BlockAPI.registerBlockSilent(block, item);
                }
            }
            var tim = (System.nanoTime() - time) / 100000;
            getLogger().log(Level.INFO, "Loaded blocks.save (" + tim / 10.0 + "ms)");
        }
    }

    @EventHandler
    private void onDisable(PluginDisableEvent event) throws IOException {
        if (event.getPlugin() instanceof BlockCore) {
            getLogger().log(Level.INFO, "Saving blocks.save");
            var config = new YamlConfiguration();
            var blocks = config.createSection("blocks");
            for (var data : BlockAPI.blocks.entrySet()) {
                var block = data.getKey();
                var world = block.getWorld();
                var key = String.format("%s_%d_%d_%d", world.getUID(), block.getX(), block.getY(), block.getZ());
                blocks.set(key, data.getValue());
            }
            config.save(new File(getDataFolder(), "blocks.save"));
            getLogger().log(Level.INFO, "Saved blocks.save");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEvent(ItemBlockFallingEvent event) {
        debugMessage(event.getClass().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEvent(ItemBlockLandingEvent event) {
        debugMessage(event.getClass().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEvent(ItemBlockMovedEvent event) {
        debugMessage(event.getClass().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEvent(ItemBlockRegisterEvent event) {
        debugMessage(event.getClass().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEvent(ItemBlockUnregisterEvent event) {
        debugMessage(event.getClass().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEvent(ItemBlockDropItemEvent event) {
        debugMessage(event.getClass().getName());
    }

    public void debugMessage(String message) {
        if (debugMode) {
            Bukkit.broadcastMessage(message);
        }
    }
}
