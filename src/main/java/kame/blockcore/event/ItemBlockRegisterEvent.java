package kame.blockcore.event;

import org.bukkit.block.Block;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

public class ItemBlockRegisterEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    private final Block block;

    private final ItemStack item;

    public ItemBlockRegisterEvent(Block block, ItemStack item) {
        this.block = block;
        this.item = item;
    }

    public Block getBlock() {
        return block;
    }

    public ItemStack getItemStack() {
        return item;
    }
}
