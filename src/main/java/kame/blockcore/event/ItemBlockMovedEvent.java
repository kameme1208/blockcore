package kame.blockcore.event;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Map;

public class ItemBlockMovedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    private final Map<Block, ItemStack> moves;

    private final BlockFace direction;

    public ItemBlockMovedEvent(Map<Block, ItemStack> moves, BlockFace direction) {
        this.moves = moves;
        this.direction = direction;
    }

    public Map<Block, ItemStack> getBlocks() {
        return Collections.unmodifiableMap(moves);
    }

    public BlockFace getDirection() {
        return direction;
    }
}
