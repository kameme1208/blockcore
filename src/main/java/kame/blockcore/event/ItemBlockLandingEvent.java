package kame.blockcore.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

public class ItemBlockLandingEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    private final Entity entity;
    private final Block block;
    private final ItemStack item;

    public ItemBlockLandingEvent(Entity entity, Block block, ItemStack item) {
        this.entity = entity;
        this.block = block;
        this.item = item;
    }

    public Entity getEntity() {
        return entity;
    }

    public Block getBlock() {
        return block;
    }

    public ItemStack getItem() {
        return item;
    }
}
